﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEnemy : MonoBehaviour
{

    // this code is used to destroy enemies that have got past the player to remove them from the game.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collision happened");
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }


    }

    
}

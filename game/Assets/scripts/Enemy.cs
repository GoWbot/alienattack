﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // this code here is the movement speed of the enmey but also its Axis that it moves along.
    public float XSpeed = 5f;
    private float YSpeed = 0f;
    Rigidbody2D rb;





    // this code is  to show the what is meant to happen when the enemy has a collision with the player character at the same time.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            Destroy(collision.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        // this  code here is the velocity spped of the enemy.
        rb.velocity = new Vector2(XSpeed, YSpeed);




    }
}

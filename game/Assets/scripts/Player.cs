﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    // this code here is the movement speed of th player but also it's vertical and horizontal movement  as well.
    Rigidbody2D rb;
    float horizontal;
    float vertical;
    public float moveSpeed = 5;

    // this code here is the fire point for the player in the game.
    public float fireRate;
    private float nextShot;
    public GameObject fireArea;



    // this code is  to show the what is meant to happen when the player has a collision with the enemy character, they are both destroy at the same time.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }
    }




    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // horizontal and vertical movement of player
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");


    }


      
    void FixedUpdate()
    {
        rb.velocity = new Vector2(horizontal * moveSpeed, vertical * moveSpeed);
    }
}

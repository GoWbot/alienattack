﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    // this part of the code has the set numbers for the speed of the bullet and what Axis the bullet will travels along.
    public GameObject Bullet; 
    public float XSpeed = 15f;
    private float YSpeed = 0f;
    private float rightBound = 50;
    Rigidbody2D rb;


    // this code is used for when the bullet collides with the enemy and they are both destoryed.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Collision happened");
        if (collision.collider.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
        }
    }


    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        // this code here is that the command code that is used for when the player presses the space bar to fire.
        rb.velocity = new Vector2(XSpeed, YSpeed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(Bullet, transform.position, Bullet.transform.rotation);

        }



    }
}
